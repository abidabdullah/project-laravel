<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
      return view('register');
    }
    public function welcome(Request $request)
    {
      // dd ($request->all());
      $nama = $request['nama'];
      $namablk = $request ['namablk'];
      return view('welcome', compact('nama','namablk'));
    }
}
