<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');
// Route::get('table');
Route::get('/',function(){
  return view('index');
});
Route::get('/table',function(){
  return view('table.table');
});
Route::get('/data-table',function(){
  return view('table.data-table');
});

// CRUD
Route::get('/cast','CastController@index'); //menampilkan list table
Route::get('/cast/create','CastController@create'); //menapilkan form membuat data
Route::post('/cast','CastController@store'); //menyimpan data
Route::get('/cast/{cast_id}','CastController@show'); //menampilkan detail
Route::get('/cast/{cast_id}/edit','CastController@edit'); //edit data
Route::put('/cast/{cast_id}','CastController@update'); //menyimpan atau update perubahan data
Route::delete('/cast/{cast_id}','CastController@destroy'); //menghapus data
