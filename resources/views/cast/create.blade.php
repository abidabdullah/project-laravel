@extends('layout.master')

@section('judul')
Cast
@endsection

@section('subjudul')
Menambahkan Data Baru
@endsection

@section('content')
<form action="/cast" method="post">
  @csrf
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="number" name="umur" class="form-control">
  </div>
  @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" rows="8" cols="80" class="form-control"></textarea>
  </div>
  @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
