@extends('layout.master')

@section('judul')
Cast
@endsection

@section('subjudul')
Edit Csat {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label>Nama</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
  </div>
  @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="number" value="{{$cast->umur}}" name="umur" class="form-control">
  </div>
  @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" rows="8" cols="80" class="form-control">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
