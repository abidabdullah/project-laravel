<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Tugas Hari ke 1</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
      @csrf
      <label for="name">First name:</label><br><br>
      <input type="text" id="name" name="nama"> <br><br>
      <label for="last name">Last name:</label><br><br>
      <input type="text" id="last name" name="namablk"><br><br>
      <label>Gender:</label><br><br>
      <input type="radio" name="jk" value="1">Male<br>
      <input type="radio" name="jk" value="2">Female<br>
      <input type="radio" name="jk" value="3">Other<br><br>
      <label>Nationality:</label><br><br>
      <select>
        <option name="nat" value="ind">Indonesia</option>
        <option name="nat" value="amr">Amerika</option>
        <option name="nat" value="ing">Inggris</option>
      </select><br><br>
      <label>Language Spoken:</label><br><br>
      <input type="checkbox" name="bhs" value="ind">Bahasa Indonesia<br>
      <input type="checkbox" name="bhs" value="eng">English<br>
      <input type="checkbox" name="bhs" value="other">Others<br><br>
      <label for="bio">Bio:</label><br><br>
      <textarea rows="15" cols="50" name="bio"></textarea><br>
      <input type="submit"value="Sign Up">
    </form>
  </body>
</html>
